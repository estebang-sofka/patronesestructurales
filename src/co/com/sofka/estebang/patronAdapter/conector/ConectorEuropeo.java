package co.com.sofka.estebang.patronAdapter.conector;

public class ConectorEuropeo {

    private int pinCuadrado;

    public ConectorEuropeo() {
    }

    public ConectorEuropeo(int pinCuadrado) {
        this.pinCuadrado = pinCuadrado;
    }

    public int getPinCuadrado() {
        return pinCuadrado;
    }
}
