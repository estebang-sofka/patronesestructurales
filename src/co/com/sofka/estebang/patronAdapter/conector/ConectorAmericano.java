package co.com.sofka.estebang.patronAdapter.conector;

public class ConectorAmericano {

    private int pinRedondo;
    private int pinCuadrado;

    public ConectorAmericano() {
    }

    public ConectorAmericano(int pinRedondo, int pinCuadrado) {
        this.pinRedondo = pinRedondo;
        this.pinCuadrado = pinCuadrado;
    }

    public int getPinRedondo() {
        return pinRedondo;
    }

    public int getPinCuadrado() {
        return pinCuadrado;
    }
}
