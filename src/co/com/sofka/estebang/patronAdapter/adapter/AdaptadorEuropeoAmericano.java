package co.com.sofka.estebang.patronAdapter.adapter;

import co.com.sofka.estebang.patronAdapter.conector.ConectorAmericano;
import co.com.sofka.estebang.patronAdapter.conector.ConectorEuropeo;

public class AdaptadorEuropeoAmericano extends ConectorAmericano {
    private ConectorEuropeo conectorEuropeo;

    public AdaptadorEuropeoAmericano(ConectorEuropeo conectorEuropeo) {
        this.conectorEuropeo = conectorEuropeo;
    }

    @Override
    public int getPinRedondo() {
        return 1;
    }

    @Override
    public int getPinCuadrado() {
        return conectorEuropeo.getPinCuadrado();
    }
}
