package co.com.sofka.estebang.patronAdapter.enchufe;

import co.com.sofka.estebang.patronAdapter.conector.ConectorAmericano;

public class EnchufeAmericano {
    private int pinRedondo;
    private int pinCuadrado;

    public EnchufeAmericano(int pinRedondo, int pinCuadrado) {
        this.pinRedondo = pinRedondo;
        this.pinCuadrado = pinCuadrado;
    }

    public int getPinRedondo() {
        return pinRedondo;
    }

    public int getPinCuadrado() {
        return pinCuadrado;
    }

    public boolean conectar(ConectorAmericano conectorAmericano) {
        boolean result;
        result = this.pinCuadrado == conectorAmericano.getPinCuadrado() &&
                this.pinRedondo == conectorAmericano.getPinRedondo();
        return result;
    }
}
