package co.com.sofka.estebang.patronFacade.facade;

import co.com.sofka.estebang.patronFacade.framework.Method;
import co.com.sofka.estebang.patronFacade.framework.MethodFactory;
import co.com.sofka.estebang.patronFacade.framework.RestConsumer;

public class RestConsumerFacade {
    public Object consumeRest(String uri, String stringMethod, Object request) {
        RestConsumer restConsumer = new RestConsumer(uri, stringMethod, request);
        Method method = MethodFactory.getMethod(restConsumer);
        return method.execute(restConsumer);
    }
}
