package co.com.sofka.estebang.patronFacade.framework;

public class PostMethod implements Method {
    @Override
    public Object execute(RestConsumer restConsumer) {
        System.out.println("Ejecutando método POST en la url: " + restConsumer.getUri());
        String response = "POST Response: {status: OK, objectCreated: " + restConsumer.getRequest() + " }";
        restConsumer.setResponse(response);
        return response;
    }
}
