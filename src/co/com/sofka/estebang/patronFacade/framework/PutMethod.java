package co.com.sofka.estebang.patronFacade.framework;

public class PutMethod implements Method {

    @Override
    public Object execute(RestConsumer restConsumer) {
        System.out.println("Ejecutando método PUT en la url: " + restConsumer.getUri());
        String response = "PUT Response: {status: OK, objectUpdated: " + restConsumer.getRequest() + " }";
        restConsumer.setResponse(response);
        return response;
    }
}
