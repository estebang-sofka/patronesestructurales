package co.com.sofka.estebang.patronFacade.framework;

public class GetMethod implements Method {
    @Override
    public Object execute(RestConsumer restConsumer) {
        System.out.println("Ejecutando método GET en la url: " + restConsumer.getUri());
        String response = "GET Response: {status: OK, objectId: " + restConsumer.getRequest() + " }";
        restConsumer.setResponse(response);
        return response;
    }
}
