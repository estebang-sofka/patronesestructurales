package co.com.sofka.estebang.patronFacade.framework;

public class MethodFactory {

    public static Method getMethod(RestConsumer restConsumer) {
        switch (restConsumer.getMethod()) {
            case "POST":
                return new PostMethod();
            case "PUT":
                return new PutMethod();
            case "GET":
            default:
                return new GetMethod();
        }
    }
}
