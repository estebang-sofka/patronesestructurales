package co.com.sofka.estebang.patronFacade.framework;

public interface Method {
    abstract Object execute(RestConsumer restConsumer);
}
