package co.com.sofka.estebang.patronFacade.framework;

public class RestConsumer {
    private String uri;
    private String method;
    private Object request;
    private Object response;

    public RestConsumer(String uri, String method, Object request) {
        this.uri = uri;
        this.method = method;
        this.request = request;
    }

    public String getUri() {
        return uri;
    }

    public String getMethod() {
        return method;
    }

    public Object getRequest() {
        return request;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
