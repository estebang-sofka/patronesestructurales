package co.com.sofka.estebang.patronDecorator.media;

import co.com.sofka.estebang.patronDecorator.model.Carro;

public interface TallerCarros {
    void repararCarro(Carro carro);
}
