package co.com.sofka.estebang.patronDecorator.media;

import co.com.sofka.estebang.patronDecorator.model.Carro;

public class TallerElectrico implements TallerCarros{

    @Override
    public void repararCarro(Carro carro) {
        carro.setArranque(true);
        carro.setRadio(true);
        carro.setLuces(true);
    }
}
