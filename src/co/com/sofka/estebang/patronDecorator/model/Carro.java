package co.com.sofka.estebang.patronDecorator.model;

public class Carro {
    private Boolean luces;
    private Boolean radio;
    private Boolean arranque;
    private Boolean llantas;
    private Boolean gases;
    private Boolean aceite;
    private Boolean cojineria;

    public Carro() {
    }

    public Carro(Boolean luces, Boolean radio, Boolean arranque, Boolean llantas, Boolean gases, Boolean aceite, Boolean cojineria) {
        this.luces = luces;
        this.radio = radio;
        this.arranque = arranque;
        this.llantas = llantas;
        this.gases = gases;
        this.aceite = aceite;
        this.cojineria = cojineria;
    }

    public Boolean getLuces() {
        return luces;
    }

    public void setLuces(Boolean luces) {
        this.luces = luces;
    }

    public Boolean getRadio() {
        return radio;
    }

    public void setRadio(Boolean radio) {
        this.radio = radio;
    }

    public Boolean getArranque() {
        return arranque;
    }

    public void setArranque(Boolean arranque) {
        this.arranque = arranque;
    }

    public Boolean getLlantas() {
        return llantas;
    }

    public void setLlantas(Boolean llantas) {
        this.llantas = llantas;
    }

    public Boolean getGases() {
        return gases;
    }

    public void setGases(Boolean gases) {
        this.gases = gases;
    }

    public Boolean getAceite() {
        return aceite;
    }

    public void setAceite(Boolean aceite) {
        this.aceite = aceite;
    }

    public Boolean getCojineria() {
        return cojineria;
    }

    public void setCojineria(Boolean cojineria) {
        this.cojineria = cojineria;
    }

    @Override
    public String toString() {
        return "Carro{" +
                "luces=" + luces +
                ", radio=" + radio +
                ", arranque=" + arranque +
                ", llantas=" + llantas +
                ", gases=" + gases +
                ", aceite=" + aceite +
                ", cojineria=" + cojineria +
                '}';
    }
}
