package co.com.sofka.estebang.patronDecorator.decorator;

import co.com.sofka.estebang.patronDecorator.media.TallerCarros;
import co.com.sofka.estebang.patronDecorator.model.Carro;

public class TallerChasis extends TallerCarrosDecorator{

    public TallerChasis(TallerCarros tallerCarros) {
        super(tallerCarros);
    }

    @Override
    public void repararCarro(Carro carro) {
        super.repararCarro(carro);
        carro.setCojineria(true);
        carro.setLlantas(true);
    }
}
