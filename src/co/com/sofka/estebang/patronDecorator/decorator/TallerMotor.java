package co.com.sofka.estebang.patronDecorator.decorator;

import co.com.sofka.estebang.patronDecorator.media.TallerCarros;
import co.com.sofka.estebang.patronDecorator.model.Carro;

public class TallerMotor extends TallerCarrosDecorator {

    public TallerMotor(TallerCarros tallerCarros) {
        super(tallerCarros);
    }

    @Override
    public void repararCarro(Carro carro) {
        super.repararCarro(carro);
        carro.setAceite(true);
        carro.setGases(true);
    }
}
