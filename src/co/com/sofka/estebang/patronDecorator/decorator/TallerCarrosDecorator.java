package co.com.sofka.estebang.patronDecorator.decorator;

import co.com.sofka.estebang.patronDecorator.media.TallerCarros;
import co.com.sofka.estebang.patronDecorator.model.Carro;

public class TallerCarrosDecorator implements TallerCarros {

    private TallerCarros tallerCarros;

    public TallerCarrosDecorator(TallerCarros tallerCarros) {
        this.tallerCarros = tallerCarros;
    }

    @Override
    public void repararCarro(Carro carro) {
        tallerCarros.repararCarro(carro);
    }
}
