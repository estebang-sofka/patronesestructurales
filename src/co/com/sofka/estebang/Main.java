package co.com.sofka.estebang;

import co.com.sofka.estebang.patronAdapter.adapter.AdaptadorEuropeoAmericano;
import co.com.sofka.estebang.patronAdapter.conector.ConectorAmericano;
import co.com.sofka.estebang.patronAdapter.conector.ConectorEuropeo;
import co.com.sofka.estebang.patronAdapter.enchufe.EnchufeAmericano;
import co.com.sofka.estebang.patronDecorator.decorator.TallerCarrosDecorator;
import co.com.sofka.estebang.patronDecorator.decorator.TallerChasis;
import co.com.sofka.estebang.patronDecorator.decorator.TallerMotor;
import co.com.sofka.estebang.patronDecorator.media.TallerElectrico;
import co.com.sofka.estebang.patronDecorator.model.Carro;
import co.com.sofka.estebang.patronFacade.facade.RestConsumerFacade;

public class Main {

    public static void main(String[] args) {
        // Patrón Adaptador
        ejecutarPatronAdaptador();

        // Patrón Decorator
        ejecutarPatronDecorator();

        // Patrón Flyweight
        ejecutarPatronFacade();
    }

    private static void ejecutarPatronFacade() {
        RestConsumerFacade restConsumerFacade1 = new RestConsumerFacade();
        System.out.println(restConsumerFacade1.consumeRest("https://facadeexample.com/api/get","GET","1"));
        System.out.println(restConsumerFacade1.consumeRest("https://facadeexample.com/api/update","PUT","{id:'1', name:'Esteban', lastName:'Garcia'}"));
        System.out.println(restConsumerFacade1.consumeRest("https://facadeexample.com/api/create","POST","{name:'Raul', lastName:'Alzate'}"));
    }

    private static void ejecutarPatronDecorator() {

        Carro carro = new Carro();

        TallerCarrosDecorator tallerCarrosDecorator = new TallerChasis(new TallerMotor(new TallerElectrico()));

        tallerCarrosDecorator.repararCarro(carro);

        System.out.println("- Input ----------------");
        System.out.println(carro);

    }

    private static void ejecutarPatronAdaptador() {
        EnchufeAmericano enchufeAmericano = new EnchufeAmericano(1, 2);
        ConectorAmericano conectorAmericano = new ConectorAmericano(1, 2);

        if (enchufeAmericano.conectar(conectorAmericano))
            System.out.println("Conectado correctamente.");

        ConectorEuropeo conectorEuropeo1 = new ConectorEuropeo(2);
        ConectorEuropeo conectorEuropeo2 = new ConectorEuropeo(3);
        //enchufeAmericano.conectar(conectorEuropeo1);

        AdaptadorEuropeoAmericano adaptadorEuropeoAmericano1 = new AdaptadorEuropeoAmericano(conectorEuropeo1);
        AdaptadorEuropeoAmericano adaptadorEuropeoAmericano2 = new AdaptadorEuropeoAmericano(conectorEuropeo2);

        System.out.println(enchufeAmericano.conectar(adaptadorEuropeoAmericano1));
        System.out.println(enchufeAmericano.conectar(adaptadorEuropeoAmericano2));
    }
}
